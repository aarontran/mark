{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Test sign convention and normalization of FFT axis coordinates for TristanFldFFT\n",
    "\n",
    "Test addresses three questions:\n",
    "* check signs for omega vs. k in FFT of a traveling wave?\n",
    "* check factors of 2 or 2pi correct for time / frequency coordinates?\n",
    "* check factors of 2 or 2pi correct for space / wavenumber coordinates?\n",
    "\n",
    "2021 March, cleaned up 2022 Jan"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from __future__ import division, print_function\n",
    "\n",
    "import matplotlib as mpl\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import scipy as sp\n",
    "import scipy.signal\n",
    "\n",
    "from mark import TristanFldFFT\n",
    "\n",
    "from fplot import colorbar\n",
    "from myutil import printmem\n",
    "\n",
    "%matplotlib inline\n",
    "%load_ext autoreload\n",
    "%autoreload 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Create a fake traveling wave\n",
    "    \n",
    "Let wavelength $\\lambda = 2 c/\\omega_p$ exactly, so k-vector norm is $\\pi \\omega_p/c$.\n",
    "+ In omp/c units, $\\lambda = 2$ and $|k| = \\pi$.\n",
    "+ In TRISTAN units, $\\lambda=20$ and $|k| = 0.1\\pi$.\n",
    "\n",
    "Let omega $\\omega = \\omega_p$ exactly.\n",
    "+ In omp/c units, omega = $1$ and period $T = 2\\pi \\approx 6.28$\n",
    "+ In TRISTAN units, omega = $\\mathtt{c}/\\mathtt{c\\_omp} = 0.045$\n",
    "  and period $T = 2\\pi/(\\mathtt{c}/\\mathtt{c\\_omp}) \\approx 140$,\n",
    "  for $\\mathtt{c\\_omp}=10$ and $\\mathtt{c}=0.45$.\n",
    "\n",
    "The resulting wave speed is $\\omega/k = 0.5 c$ in physical units.\n",
    "+ In omp/c units, the wave speed is $1/\\pi \\approx 0.318$\n",
    "+ In TRISTAN units, the wave speed is $0.045/(10\\pi) \\approx 0.00143$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class FakeTristanRun():\n",
    "    def __init__(self):\n",
    "        self.input = dict()\n",
    "    def c_omp(self):\n",
    "        return self.input['c_omp']\n",
    "    def omp(self):\n",
    "        return self.input['c']/self.input['c_comp']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def my_wave(t, x, y, z, c=0.45, c_omp=10):\n",
    "    \"\"\"input units must be in terms of TRISTAN units\"\"\"\n",
    "    \n",
    "    wave_lambda = 2*c_omp  # in tristan units\n",
    "    omega = 1*(c/c_omp)  # in tristan units\n",
    "    \n",
    "    thetaxy = 25*np.pi/180\n",
    "    \n",
    "    kmag = 2*np.pi/wave_lambda\n",
    "    kx = kmag*np.cos(thetaxy)\n",
    "    ky = kmag*np.sin(thetaxy)\n",
    "    kz = 0.0\n",
    "    \n",
    "    return np.sin(kx*x + ky*y + kz*z - omega*t)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "run = FakeTristanRun()\n",
    "run.input['interval'] = 20\n",
    "run.input['istep'] = 1\n",
    "run.input['c_omp'] = 10\n",
    "run.input['c'] = 0.45"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dimf = (1000,200,1)\n",
    "nscene = 100\n",
    "\n",
    "tvec = np.arange(nscene) * run.input['interval']\n",
    "xvec = np.arange(dimf[0]) * run.input['istep']\n",
    "yvec = np.arange(dimf[1]) * run.input['istep']\n",
    "zvec = np.arange(dimf[2]) * run.input['istep']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tt, xx, yy, zz = np.meshgrid(tvec, xvec, yvec, zvec, indexing='ij')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fld = my_wave(tt,xx,yy,zz)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "printmem()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# show three time snapshots to inspect field\n",
    "# make sure it is really traveling in expected direction (look at the corners of the plot)\n",
    "# and that we are not (obviously) aliasing\n",
    "for ii in [0, 1, 2]:\n",
    "    plt.imshow(fld[ii][:,:,0].T, origin='lower')\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Perform FFT on field, first setup some helper functions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def setup_plot(axes, ft, bnd_omkx=(), bnd_omky=(), bnd_kxky=()):\n",
    "\n",
    "    plt.sca(axes[0])\n",
    "    # note that imshxy does transpose too, so the transposes cancel out.  a bit confusing.\n",
    "    plt.imshow(\n",
    "        ft.power_omkx**0.5, origin='lower',\n",
    "        norm=mpl.colors.LogNorm(*bnd_omkx),\n",
    "        extent=(\n",
    "            ft.kx[0], ft.kx[-1],\n",
    "            ft.om[0], ft.om[-1],\n",
    "        )\n",
    "    )\n",
    "    plt.gca().set_aspect('auto')\n",
    "    plt.colorbar()\n",
    "    plt.xlabel('kx')\n",
    "    plt.ylabel('omega')\n",
    "    plt.title('kx-omega sqrt(power) spectrum')\n",
    "\n",
    "    plt.sca(axes[1])\n",
    "    plt.imshow(\n",
    "        ft.power_omky**0.5, origin='lower',\n",
    "        norm=mpl.colors.LogNorm(*bnd_omky),\n",
    "        extent=(\n",
    "            ft.ky[0], ft.ky[-1],\n",
    "            ft.om[0], ft.om[-1],\n",
    "        )\n",
    "    )\n",
    "    plt.gca().set_aspect('auto')\n",
    "    plt.colorbar()\n",
    "    plt.xlabel('ky')\n",
    "    plt.ylabel('omega')\n",
    "    plt.title('ky-omega sqrt(power) spectrum')\n",
    "\n",
    "\n",
    "    plt.sca(axes[2])\n",
    "    plt.imshow(\n",
    "        ft.power_kxky.T**0.5, origin='lower',\n",
    "        norm=mpl.colors.LogNorm(*bnd_kxky),\n",
    "        extent=(\n",
    "            ft.kx[0], ft.kx[-1],\n",
    "            ft.ky[0], ft.ky[-1],\n",
    "        )\n",
    "    )\n",
    "    plt.gca().set_aspect('equal')\n",
    "    plt.colorbar()\n",
    "    plt.xlabel('kx')\n",
    "    plt.ylabel('ky')\n",
    "    plt.title('kx-ky sqrt(power) spectrum')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Do FFT on field using omp, c/omp units, then verify k, omega, wavespeeds"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ft = TristanFldFFT(run, fld=fld)\n",
    "ft.transform(\n",
    "    twindow = sp.signal.windows.hann,\n",
    "    xwindow = sp.signal.windows.hann,\n",
    "    ywindow = sp.signal.windows.hann,\n",
    "    verbose = True,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "kx_expected = np.pi * np.cos(25.*np.pi/180)  # in c/omp units\n",
    "ky_expected = np.pi * np.sin(25.*np.pi/180)  # in c/omp units\n",
    "omega_expected = 1\n",
    "vx_expected = omega_expected/kx_expected\n",
    "vy_expected = omega_expected/ky_expected"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = plt.subplots(1,3,figsize=(25,8))\n",
    "setup_plot(\n",
    "    axes, ft,\n",
    "    #bnd_omkx=(1e-1,1e5),\n",
    "    #bnd_omky=(1e-1,1e5),\n",
    "    #bnd_kxky=(1e-1,1e5),\n",
    ")\n",
    "\n",
    "plt.sca(axes[0])\n",
    "kvec = np.linspace(0, +10, 100)\n",
    "plt.plot(kvec, vx_expected * kvec, c='r', lw=1)\n",
    "plt.axvline(kx_expected, ls=':', c='k', lw=1.5)\n",
    "plt.axhline(omega_expected, ls=':', c='k', lw=1.5)\n",
    "plt.xlim(ft.kx[0], ft.kx[-1])\n",
    "plt.ylim(ft.om[0], ft.om[-1])\n",
    "\n",
    "plt.sca(axes[1])\n",
    "kvec = np.linspace(0, +10, 100)\n",
    "plt.plot(kvec, vy_expected * kvec, c='r', lw=1)\n",
    "plt.axvline(ky_expected, ls=':', c='k', lw=1.5)\n",
    "plt.axhline(omega_expected, ls=':', c='k', lw=1.5)\n",
    "plt.xlim(ft.ky[0], ft.ky[-1])\n",
    "plt.ylim(ft.om[0], ft.om[-1])\n",
    "\n",
    "plt.sca(axes[2])\n",
    "kvec = np.linspace(0, +10, 100)\n",
    "plt.plot(kvec, (ky_expected/kx_expected) * kvec, c='r', lw=1)\n",
    "plt.axvline(kx_expected, ls=':', c='k', lw=1.5)\n",
    "plt.axhline(ky_expected, ls=':', c='k', lw=1.5)\n",
    "plt.xlim(ft.kx[0], ft.kx[-1])\n",
    "plt.ylim(ft.ky[0], ft.ky[-1])\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Same: test FFT on fields, but now work using TRISTAN units"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ft = TristanFldFFT(\n",
    "    run,\n",
    "    fld=fld,\n",
    "    omega_conv = 1.,  # one tristan time unit\n",
    "    space_conv = 1.,  # one tristan cell\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ft.transform(\n",
    "    twindow = sp.signal.windows.hann,\n",
    "    xwindow = sp.signal.windows.hann,\n",
    "    ywindow = sp.signal.windows.hann,\n",
    "    verbose = True,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Do we have the correct Nyquist frequencies in both time and space??\n",
    "* run input interval = 20, fsamp = 1/2.  Nyquist signal has T = 40 laps (fnyquist = 1/40 Hz).  Angular freq is 2 pi f = 0.157\n",
    "* run istep = 1, ksamp = 1/1.  Nyquist signal has wavelength 2 cells, so knyquist = pi."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "kx_expected = 0.1*np.pi * np.cos(25.*np.pi/180)  # in c/omp units\n",
    "ky_expected = 0.1*np.pi * np.sin(25.*np.pi/180)  # in c/omp units\n",
    "omega_expected = run.input['c']/run.input['c_omp']\n",
    "vx_expected = omega_expected/kx_expected\n",
    "vy_expected = omega_expected/ky_expected"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = plt.subplots(1,3,figsize=(20,6))\n",
    "setup_plot(axes, ft)\n",
    "\n",
    "plt.sca(axes[0])\n",
    "kvec = np.linspace(0, 0.8, 100)\n",
    "plt.plot(kvec, vx_expected * kvec, c='r', lw=1)\n",
    "plt.axvline(kx_expected, ls=':', c='k', lw=1.5)\n",
    "plt.axhline(omega_expected, ls=':', c='k', lw=1.5)\n",
    "\n",
    "plt.sca(axes[1])\n",
    "kvec = np.linspace(0, 0.45, 100)\n",
    "plt.plot(kvec, vy_expected * kvec, c='r', lw=1)\n",
    "plt.axvline(ky_expected, ls=':', c='k', lw=1.5)\n",
    "plt.axhline(omega_expected, ls=':', c='k', lw=1.5)\n",
    "\n",
    "plt.sca(axes[2])\n",
    "kvec = np.linspace(0, 1, 100)\n",
    "plt.plot(kvec, (ky_expected/kx_expected) * kvec, c='r', lw=1)\n",
    "plt.axvline(kx_expected, ls=':', c='k', lw=1.5)\n",
    "plt.axhline(ky_expected, ls=':', c='k', lw=1.5)\n",
    "\n",
    "plt.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
