#!/usr/bin/env python3
"""
Library of useful tensor stack (field) related functions, with particular
application to TRISTAN-MP analysis or general plasma simulation study.

Much credit to numpy.einsum for its extreme expressiveness!

Brief Markdown description of tensor indexing convention follows.

## Tensor indexing convention

A matrix (tensor) stack has spatial indices (x,y,z) and tensor indices (i,j).
How shall we order the corresponding Numpy array indices?  We have two choices:

    (x, y, z, ...; i, j, k, ...)  # space indices first
    (i, j, k, ...; x, y, z, ...)  # tensor indices first

In my code, tensor indices precede space indices.

_Example:_ how do I store four-velocity vectors on a 2-D domain?
Suppose my grid is (100 x 10), very elongated in x direction.

    >>> c = np.ones((100,10))  # same shape for vx, vy, vz
    >>> vx = np.random.random((100,10)) / (3**0.5)
    >>> vy = np.random.random((100,10)) / (3**0.5)
    >>> vz = np.random.random((100,10)) / (3**0.5)
    >>> gamma = 1 / (1 - (vx**2 + vy**2 + vz**2))**0.5

    >>> v4 = np.array([gamma*c, gamma*vx, gamma*vy, gamma*vz])

    >>> print(v4.shape)
    (4, 100, 10)

    >>> print(v4[:,0,0])
    [ 1.06083882  0.0372386   0.33035826  0.12188403]

_Example:_ typical index structures.

    # 1-D domain
    (x)        # scalar
    (i; x)     # vector
    (i, j; x)  # tensor (2-index)

    # 2-D domain
    (x, y)        # scalar
    (i, j; x, y)  # tensor (2-index)

_Example:_ more use cases.

    Ttensi[:,:,0]  # view tensor at specific  location (1-D)
    Ttensi[:,:,0,0,0]  # view tensor at specific location (3-D)

    np.einsum('i..., j...', vi, vi)  # vector, outer product
    np.einsum('...ai, ...bj, ...ij', rotation, rotation, pressure)  #  tensor rotation (change of basis)

    # Easy to construct manually from 1-D scalar components
    # Of course the very fact that we use Numpy requires that we've assumed Cartesian basis . . .

    Ttensi = np.array([[Txxi, Txyi, Txzi],
                       [Tyxi, Tyyi, Tyzi],
                       [Tzxi, Tzyi, Tzzi]])

__Corollary:__ input subscripts in `np.einsum` should always be written `ijk...` rather than `...ijk`.

The main benefit of (tensor; space) ordering is the ease of constructing and extracting tensor components.
In the examples above, we can quickly retrieve component scalar fields:

    # Four-velocity
    v0 = v4[0]
    v1 = v4[1]
    # Temperature tensor
    Txyi = Ttensi[0,1]

The downsides are:
* We cannot quickly view single tensors or vectors, unlike `v[0]` or `v[0,0,0]` with (space, tensor) ordering.
  For (tensor; space) ordering, an explicit `:` over tensor indices is required.
* The (tensor,space) index ordering is not compatible with matmul(...) in Numpy 1.6.

In my view, the benefit is much better than the downsides.
"""

import numpy as np


def rotmatrix(old, new):
    """Return 3x3 matrix that rotates old->new
    Input: 3-vectors OR 3-vector stacks (must be consistent)
    Output: 3x3 matrix OR 3x3 matrix stack
    """
    k = np.cross(old, new, axisa=0, axisb=0, axisc=0)  # rotation axis
    angle = np.arccos(np.einsum('i...,i...', old, new)
                      / (np.linalg.norm(old,axis=0) * np.linalg.norm(new,axis=0)) )

    kx, ky, kz = k/np.linalg.norm(k, axis=0)
    o = np.zeros_like(kx)
    m = np.array([[  o, -kz,  ky],
                  [ kz,   o, -kx],
                  [-ky,  kx,   o]])  # o represents zero (0)

    # Construct broadcasted identity
    eye = np.zeros_like(m)
    for i in range(3):
        eye[i,i] = 1

    # Rodrigues rotation formula: I + sin(theta) * M + (1-cos(theta)) * M^2
    rotor = eye + np.sin(angle) * m + (1 - np.cos(angle)) * np.einsum('ij...,jk...->ik...', m, m)  # m^2

    # fix edge case where old=new exactly, so k=0 and kx,ky,kz=NaN because of
    # zero/zero division which yields NaNs in output matrix
    sel = np.all(k == 0., axis=0)
    if np.any(sel):
        if len(old.shape) == 2:
            rotor = eye
        else:
            rotor[:,:,sel] = eye[:,:,sel]

    return rotor


def lorentz(v):
    """Return Lorentz boost for input three-velocity vector(s), normalized to speed of light
    Operates cleanly over velocity field ("stack"), where tensor index precedes space indices
    I.e., shape: (i; x, y, z, ...) is allowed

    ~~~~~~
    Sign convention: suppose that we apply Lorentz boost \Lambda(v)_{ij} to the
    same velocity vector v_{j} as

        v'_{i} = \Lambda(v)_{ij} v_{j} .

    The result v'_{i} is exactly zero, rather than ~2*v (in Galilean limit).
    The Lorentz boost is a passive transformation; boosting in the same
    direction as a given velocity vector diminishes the velocity vector's
    magnitude in the boosted frame.
    ~~~~~~
    Sign convention, part 2: the tensor maps contravariant vectors to the
    primed coordinate frame,

        x^{\alpha'} = \Lambda^{\alpha'}_{\alpha} x^{\alpha}

    where particles moving with speed v^{i} in unprimed frame, will be
    stationary in primed frame.
    ~~~~~~
    Sign convention, part 3: for boost along one coordinate axis,
    v = [β, 0, 0] with β > 0, the tensor looks like:

        [[   γ, -β*γ],
         [-β*γ,    γ]]

    where γ = 1/(1-β**2)**0.5.
    ~~~~~~

    Input:
        ndarray of shape (3,) or (3,x,y,z,...)
    Output:
        ndarray of shape (4,4) or (4,4,x,y,z,...)
    """
    vx, vy, vz = v
    v0 = (vx**2 + vy**2 + vz**2)**0.5
    gam = 1 / (1 - (vx**2 + vy**2 + vz**2))**0.5
    #assert np.max(v0) < 1  # skip assert to let NaNs pass thru cleanly

    # Broken-apart code doesn't work.
    # must replace 1, 0 with np.ones_like(...), np.zeros_like(...)
    # OR use other matrix construction methods, then explicitly set indices
    # (i suspect the latter is faster)
    #part0 = np.array([[  1, -vx, -vy, -vz],
    #                  [-vx,   1,   0,   0],
    #                  [-vy,   0,   1,   0],
    #                  [-vz,   0,   0,   1]
    #                 ])
    #part1 = np.array([[0,     0,     0,     0],
    #                  [0, vx*vx, vx*vy, vx*vz],
    #                  [0, vy*vx, vy*vy, vy*vz],
    #                  [0, vz*vx, vz*vy, vz*vz]
    #                 ])
    #boost = gam*part0 + (gam-1)/v0**2*part1

    boost = np.array([[ gam,                  -gam*vx,               -gam*vy,               -gam*vz],
                      [-gam*vx, 1+(gam-1)*vx*vx/v0**2,   (gam-1)*vx*vy/v0**2,   (gam-1)*vx*vz/v0**2],
                      [-gam*vy,   (gam-1)*vy*vx/v0**2, 1+(gam-1)*vy*vy/v0**2,   (gam-1)*vy*vz/v0**2],
                      [-gam*vz,   (gam-1)*vz*vx/v0**2,   (gam-1)*vz*vy/v0**2, 1+(gam-1)*vz*vz/v0**2]
                     ])

    # Apply mask to fix regions where v=0 exactly and NaNs appear
    # in boost due to zero/zero division --ATr,2024sep02
    sel = v0==0
    for ii in range(1,3+1):
        for jj in range(1,3+1):
            if ii == jj:
                boost[ii,jj][sel] = 1.
            else:
                boost[ii,jj][sel] = 0.

    return boost

def boost_em_flds(e, b, v3):
    """
    Boost electromagnetic fields from one frame into another

    Lorentz boost convention for v3, follows that of lorentz(...)
    (a particle moving with velocity v3 in input/unprimed frame will be
    stationary in the output/primd frame)

    Input:
        e = electric field, ndarray of shape (3, ...)
        b = magnetic field, ndarray of shape (3, ...)
        v = three-velocity, ndarray of shape (3,) or (3, ...)
    Output:
        e, b = boosted electric, magnetic fields, same shapes as input
    """
    ex, ey, ez = e
    bx, by, bz = b
    o = np.zeros_like(ex)

    # contravariant EM(faraday) tensor, F^{\alpha\beta}
    # metric is (-,+,+,+)
    f = np.array([[  o,  ex,  ey,  ez],
                  [-ex,   o,  bz, -by],
                  [-ey, -bz,   o,  bx],
                  [-ez,  by, -bx,   o]])

    boost = lorentz(v3)
    f_boost = np.einsum('ai...,bj...,ij...->ab...', boost, boost, f)

    ex_boost = f_boost[0,1]
    ey_boost = f_boost[0,2]
    ez_boost = f_boost[0,3]

    bx_boost = f_boost[2,3]
    by_boost = f_boost[3,1]
    bz_boost = f_boost[1,2]

    e_boost = np.array([ex_boost, ey_boost, ez_boost])
    b_boost = np.array([bx_boost, by_boost, bz_boost])

    return e_boost, b_boost


if __name__ == '__main__':
    pass

