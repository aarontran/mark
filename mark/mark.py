#!/usr/bin/env python3

from __future__ import division

from collections.abc import Mapping, MutableMapping, Sequence
#from copy import deepcopy
from glob import glob
from datetime import datetime
import h5py
import matplotlib.pyplot as plt
import numpy as np
import os
from os import path
import re
import warnings
from warnings import warn

warnings.simplefilter('always', UserWarning)

NGHOST = 2  # number of left-side ghost cells in simulation

class TristanRun(Sequence):
    """TRISTAN-MP run representation (directory, input, outputs)"""

    def __init__(self, rundir, outdir="output"):
        """TRISTAN-MP run representation (directory, input, outputs)"""

        self.rundir = rundir
        self.input = TristanInput(path.join(rundir, "input"))  # may not exist
        self.outdir = path.join(rundir, outdir)

        self._scenes = []

        # sort by numeric order of param filenames, casting strings to int(...)
        # to avoid any problems from uneven zero-padding
        params = glob(path.join(self.outdir, "param.*"))
        params = sorted(params, key=lambda x: int(x.split('.')[-1]))

        # 2021 jan 09 -- removed some checks/warnings for scene numbering,
        # to allow for runs w/ all scenes deleted except last,
        # every other scene deleted, et cetera...

        for param in params:
            # this is robust against missing outputs
            # e.g., missing .000, or random gaps...
            numstr = param.split('.')[-1]  # grab param.* number
            try:
                n = int(numstr)  # grab param.* number
                self._scenes.append(Scene(n, self.outdir))
            except ValueError:  # probably "param.***" for bad outputs...
                continue

        #self.started = len(self) >= 2  # Got both *.000 and *.001 dumps
        self.done = False
        if len(self) > 0 and self[-1].n > 0:  # avoid the .000 dump
            if 'last' in self.input:
                n_expected = 1 + int((self.input['last'] - self.input['pltstart']) / self.input['interval'])
                self.done = self[-1].n >= n_expected
            else:
                warn("unexpectedly missing param 'last' in {}".format(self.input.fname))

    def __getitem__(self, index):
        # cribbed slicing from
        # http://stupidpythonideas.blogspot.com/2015/07/creating-new-sequence-type-is-easy.html
        # might break some logic in MkShock... yes, it does...
        #if isinstance(index, slice):
        #    rsliced = type(self)(self.rundir)#, outdir=path.relpath(self.outdir, start=self.rundir))  # rather fragile
        #    rsliced._scenes = rsliced._scenes[index]  # slice directly into list
        #else:
        return self._scenes[index]

    def __len__(self):
        return len(self._scenes)

    @property
    def has_restarts(self):
        """Report if any restart files present"""
        restflds = glob(path.join(self.rundir, "restartflds", "restflds.*"))
        restprtl = glob(path.join(self.rundir, "restartprtl", "restprtl.*"))
        if restflds or restprtl:
            return True  # to explicitly give boolean rather than list

    @property
    def mi_me(self):
        """Report mass ratio from input, which is used very often"""
        return self.input['mi']/self.input['me']

    @property
    def started(self):
        """Has this run started? (at least one output, or input file exists on
        disk and has read-only file permissions)"""
        return len(self) > 0 or (path.isfile(self.input.fname)
                                 and not os.access(self.input.fname, os.W_OK))

    def collate_entropy(self, *args, **kwargs):
        """Alias for better naming
        Keeping get_entropy(...) around for backwards compatibility
        """
        return self.get_entropy(*args, **kwargs)

    def get_entropy(self, *want, squeeze=False):
        """Get entropy calculation

        Convenience wrapper to Scene.entropy(...)

        WARNING: critical assumption that dimension of LAST dump is "big"
        enough for everything preceding.  Position of values within array may
        be wrong for anything other than "right-side" enlarge in a given axis.

        Input: want = which outputs to get
        Output: buncha numpy arrays...
        """

        allflds = {}

        for scene in reversed(self):  # start at n=len(self)-1
            for key, fld in scene.entropy(*want).items():

                if key not in allflds:  # initialize
                    base = np.full_like(fld, np.nan, dtype=fld.dtype)  # no type casting
                    # Expand (x) to (t,x), then broadcast along new time axis
                    # Xinyi's entropy calculation has only one axis
                    base = np.expand_dims(base, axis=-2)
                    base = np.repeat(base, len(self), axis=-2)
                    allflds[key] = base

                if len(fld.shape) == 1:  # scalar
                    allflds[key][scene.n, 0:fld.shape[-1]] = fld
                else:
                    raise Exception("Bad entropy fld shape: {}".format(fld.shape))

        if squeeze:
            for key in allflds:
                allflds[key] = np.squeeze(allflds[key])

        return allflds

    def collate_flds(self, *want, avgaxis=(), tsel=None, verbose=False, **kwargs):
        """Collate 1-D-averaged flds from all output dumps
        Convenience wrapper to Scene.flds(...)

        Time index (t) is inserted between tensor and space indices
            Rank-2 tensors: (i, j; t, x)
            Vectors:        (i; t, x)
            Scalars:        (t, x)

        WARNING: critical assumption that dimf of LAST dump is "big" enough for
        everything preceding.  Position of values within array may be wrong for
        anything other than "right-side" enlarge in a given axis.

        Timing: ~140 msec for just 'densi', mi49Ms4betap0.25theta90_2d_dprec
        or similar 2-D xshock run

        Input:
            *want = which outputs to get
            avgaxis = which axes to collapse (passed to np.mean(...))
            tsel = select time axis interval, alleviates memory usage
                Accepts: slice(...), list of numbers
                WARNING: tsel=[1,5] will only get snapshots 001, 005, NOT the range 001-005.
                    use slice notation to get full range!)
            **kwargs = additional kwargs passed to Scene.flds(...)
                Especially useful for passing space filters
        Output: buncha numpy arrays...
        """
        # Enhancement -- allow density weighting.  See bulk_plot_box1d.py code for this.
        # wt array shape must be (i, j; t, x, y, z)
        # I think the only reasonable option is to use existing fld, densi/e
        # as keyword for this.

        allflds = {}

        # iterate from last->first output to get largest array shape
        if tsel is None:
            scenes = reversed(self)
        elif type(tsel) == slice:
            if tsel.step is not None and tsel.step < 0:
                scenes = self[tsel]
            else:
                scenes = reversed(self[tsel])
        elif type(tsel) == int:
            scenes = [self[tsel]]  # keep in a list
        elif type(tsel) == list:
            scenes = [self[x] for x in reversed(sorted(tsel))]
        scenes = list(scenes)

        if verbose:
            started = datetime.now()
            print("Starting to collate flds (iterates last to first)")

        for cnt, scene in enumerate(scenes):
            pos = len(scenes) - cnt - 1  # = scene.n if no time slicing
            for key, fld in scene.flds(*want, squeeze=False, **kwargs).items():

                avgfld = np.mean(fld, axis=avgaxis)

                # initialize to largest possible size
                if key not in allflds:
                    base = np.full_like(avgfld, np.nan, dtype=avgfld.dtype)  # no type casting
                    # Expand (i,j,k,...; x,y,z) to (...; t, x,y,z),
                    # then broadcast along new time axis
                    base = np.expand_dims(base, axis=-4+len(avgaxis))
                    base = np.repeat(base, len(scenes), axis=-4+len(avgaxis))
                    allflds[key] = base

                # insert averaged field
                # Ex: for rank-2 tensor field with avgaxis=(-1), sel slices like
                # [:, :, scene.n, 0:avgfld.shape[-2], 0:avgfld.shape[-1]]
                sel = list(map(slice, avgfld.shape))
                sel.insert(-3 + len(avgaxis), pos)
                sel = tuple(sel)  # using list vs tuple is ambiguous, may be
                # treated as array indices for one axis versus slices into
                # multiple axes
                allflds[key][sel] = avgfld

            if verbose:
                print("collated scene", scene.n, "elapsed", datetime.now() - started)

        return allflds

    def get_flds(self, *want, squeeze=False, verbose=False):
        """Unpack and organize TRISTAN flds from ALL output dumps

        Convenience wrapper to Scene.flds(...)

        WARNING: critical assumption that dimf of LAST dump is "big" enough for
        everything preceding.  Position of values within array may be wrong for
        anything other than "right-side" enlarge in a given axis.

        WARNING: DEPRECATED, USE NEW collate_flds(...)

        Input: want = which outputs to get
        Output: buncha numpy arrays...

          Warning: time index (t) is inserted between tensor and space indices
            Tensors: (i, j; t, x, y, z)
            Vectors: (i; t, x, y, z)
            Scalars: (t, x, y, z)
        """
        allflds = {}

        for n in reversed(range(len(self))):  # start at n=len(self)-1

            if verbose:
                print("Loading flds.tot.{:3d} . . .".format(n), end='')

            flds = self[n].flds(*want, squeeze=False)

            for key, fld in flds.items():

                if key not in allflds:  # initialize
                    base = np.full_like(fld, np.nan, dtype=fld.dtype)  # NO type casting - leave to user discretion
                    # Expand (i,j,k,...; x,y,z) to (...; t, x,y,z),
                    # then broadcast along new time axis
                    base = np.expand_dims(base, axis=-4)
                    base = np.repeat(base, len(self), axis=-4)
                    allflds[key] = base
                    #print("Initialize from:", key, fld.shape)
                    #print("     to allflds:", key, base.shape)

                #print("Saving", key, "at scene=", n)
                #print("    insert", fld.shape)
                #print("    to    ", allflds[key].shape)
                if len(fld.shape) == 3:  # scalar
                    allflds[key][n,
                                 0:fld.shape[-3],
                                 0:fld.shape[-2],
                                 0:fld.shape[-1]] = fld
                elif len(fld.shape) == 4:  # vector
                    allflds[key][:,n,
                                 0:fld.shape[-3],
                                 0:fld.shape[-2],
                                 0:fld.shape[-1]] = fld
                elif len(fld.shape) == 5:  # rank-2 tensor
                    allflds[key][:,:,n,
                                 0:fld.shape[-3],
                                 0:fld.shape[-2],
                                 0:fld.shape[-1]] = fld
                else:
                    raise Exception("Bad fld shape: {}".format(fld.shape))

            if verbose:
                print(" done!")

        if squeeze:
            for key in allflds:
                allflds[key] = np.squeeze(allflds[key])

        return allflds

    def when(self, t, method='slow'):
        """Return scene closest to the requested time (in laps)"""
        pltstart = self.input['pltstart']
        intv = self.input['interval']

        if method == 'slow':
            # Slower cuz we have to read all the param HDF5 file
            # but, more guaranteed to be correct incase of uneven time spacing
            times = np.array([s.lap() for s in self])

        elif method == 'fast':
            assert self[0].lap() == pltstart  # sanity check
            assert self[-1].lap() == pltstart + self[-1].n * intv  # sanity check

            times = pltstart + np.arange(len(self)) * intv
            # sanity check
            # lap() method also uses pltstart / intv, but it ALSO checks the
            # param 'time' to make sure nothing is amiss...
            if times[0] <= 1:  # prevent divide by zero
                assert abs(times[0] - self[0].lap()) < 1e-10
            else:
                assert abs(times[0] - self[0].lap())/times[0] < 1e-10
            # more sanity check
            assert abs(times[-1] - self[-1].lap())/times[-1] < 1e-10

        # better be ascending
        assert len(times) == len(self)
        assert np.all(np.diff(times) > 0)

        # i'm being a bit sloppy about scene index "n" versus
        # array index, which could differ if, e.g., user manually decimates the
        # outputs to save space, but still wants to access results in a time
        # series... right now that kind of decimation is not supported
        i = np.searchsorted(times, t, side='left')
        if i == 0:
            return self[0]
        elif i == len(times):
            return self[-1]
        else:  # between two array elements with zach galifianakis
            if abs(t-times[i-1]) < abs(times[i]-t):
                return self[i-1]
            else:
                return self[i]


class Scene(object):
    """TRISTAN-MP scene (output dump) representation

    HDF5 read is slow, dominates I/O.
    Once loaded to numpy arrays, manipulation is very fast.
    Philosophy: load and cache as needed, used cached copies wherever possible.
    Avoid accessing via HDF5 file object.
    """

    def __init__(self, n, outdir):
        """TRISTAN-MP scene"""
        self.n = n
        self.outdir = outdir
        self.param = TristanParam(path.join(outdir, "param.{:03d}".format(n)))

    def dimf(self, istep=None):
        """
        Input: N/A
        Output: dimf tuple, representing size of downsampled field outputs
        """
        if istep is None:
            istep = self.param['istep']
        mx0, my0, mz0 = (self.param['mx0'],
                         self.param['my0'],
                         self.param['mz0'])
        dimf = (max(np.ceil((mx0-5)/istep), 1),
                max(np.ceil((my0-5)/istep), 1),
                max(np.ceil((mz0-5)/istep), 1)
               )  # Copy of TRISTAN output.F90 code
        return tuple(map(int, dimf))  # convert to int

    def lap(self):
        """Get TRISTAN lap of given snapshot, assumes even spacing"""
        mylap = self.param['pltstart'] + self.n * self.param['interval']
        # "time" is single prec, not OK for runs w/ many millions of laps,
        # but it should agree w/ our exact integer estimate of lap
        mytime = self.param['time'] / (self.param['c']/self.param['c_omp'])
        def approx_equal(x, y, releps=1e-6):
            """Verify to releps=1e-6 for single prec"""
            if abs(min(x,y)) < 1:
                return abs(x - y) < releps
            return abs(x - y) / abs(min(x, y)) < releps
        if not approx_equal(mylap, mytime):
            raise Exception("mylap={}, mytime={}".format(mylap,mytime))
        return mylap

    def entropy(self, *want):
        """Unpack and organize TRISTAN entropy outputs

        Input: requested output names.  Valid are...
            entropyx, xsl, entropyx_loc, entropyxi_loc, xsl_loc
        Output: buncha numpy arrays...
        """
        if not want:
            want = ['entropyx', 'entropyx_loc', 'entropyxi_loc',
                    'xsl_loc']

        out = {}

        fname = path.join(self.outdir, "jointmom.{:03d}".format(self.n))
        if 'entropyx' in want or 'xsl' in want:
            with h5py.File(fname, 'r') as f:
                if 'entropyx' in want:
                    out['entropyx'] = f['entropyx'][()]
                if 'xsl' in want:
                    out['xsl'] = f['xsl'][()]

        fname = path.join(self.outdir, "jointmomloc.{:03d}".format(self.n))
        if 'entropyx_loc' in want or 'entropyxi_loc' in want or 'xsl_loc' in want:
            with h5py.File(fname, 'r') as f:
                if 'entropyx_loc' in want:
                    out['entropyx_loc'] = f['entropyx'][()]
                if 'entropyxi_loc' in want:
                    out['entropyxi_loc'] = f['entropyxi'][()]
                if 'xsl_loc' in want:
                    out['xsl_loc'] = f['xsl'][()]

        return out

    def flds(self, *want, xsel=slice(None), ysel=slice(None), zsel=slice(None),
             squeeze=False, stem='flds.tot', fname=None):
        """Unpack and organize TRISTAN flds outputs

        Different objects have different shapes:
          Tensors: (i, j; x, y, z)
          Vectors: (i; x, y, z)
          Scalars: (x, y, z)

        Input: requested output names.  Valid are...
                Ttensi, Ttense
                b, e
                v3i, v3e, v4i, v4e
                dens, densi, dense
                or any other scalar field w/ same dimf contained in flds file
            kwargs:
                squeeze - squeeze out extra dimensions in x,y,z or not?
                xsel - select array indices in x-coordinate?
                ysel - select array indices in y-coordinate?
                zsel - select array indices in z-coordinate?
                stem - file name prefix, assumed in run output directory
                fname - custom filename, must have matched dimf with scene,
                    user should specify full path
        Output: buncha numpy arrays...
        """
        if not want:
            #want = ['Ttensi', 'Ttense', 'b', 'v3i', 'v3e', 'densi', 'dense']
            want = ['b', 'v3i', 'v3e', 'densi', 'dense']

        shape = {'Ttensi': (3, 3),  # only if input savetmp=1
                 'Ttense': (3, 3),  # only if input savetmp=1
                 'Ttens': (3, 3),  # only if input savetmp=1
                 'b': (3,),
                 'e': (3,),
                 'bx': (),
                 'by': (),
                 'bz': (),
                 'ex': (),
                 'ey': (),
                 'ez': (),
                 'j': (3,),
                 'v3i': (3,),
                 'v3e': (3,),
                 'v4i': (4,),
                 'v4e': (4,),
                 'dens': (),
                 'densi': (),
                 'dense': (),
                 'kene': (),
                 'keni': (),
                }
        for arg in want:
            if arg not in shape:
                shape[arg] = ()

        out = {}
        dimf = self.dimf()
        for arg in want:
            # np.empty(...) takes 1e-9 to 1e-6 sec/array;
            # np.ones(...) takes 1e-6 to 1e0 sec/array.
            out[arg] = np.empty((*shape[arg], *dimf))

        if fname is None:
            fname = path.join(self.outdir, "{:s}.{:03d}".format(stem, self.n))
        with h5py.File(fname, 'r') as f:

            # Fortran uses column-major ordering, and HDF5 transposes
            # dimensions during file write to keep array contiguous.
            # Thus row-major order tools (HDF5 utilities, C, Python) see the
            # dimensions transposed; undo to get usual x,y,z order.

            for kk in want:
                if kk == 'Ttensi':
                    Txxi = f['tmpxxi'][zsel,ysel,xsel]
                    Tyyi = f['tmpyyi'][zsel,ysel,xsel]
                    Tzzi = f['tmpzzi'][zsel,ysel,xsel]
                    Txyi = f['tmpxyi'][zsel,ysel,xsel]
                    Txzi = f['tmpxzi'][zsel,ysel,xsel]
                    Tyzi = f['tmpyzi'][zsel,ysel,xsel]
                    Tyxi = Txyi  # these are views; updates to Tyxi also alter Txyi
                    Tzxi = Txzi
                    Tzyi = Tyzi
                    out['Ttensi'] = np.array([[Txxi, Txyi, Txzi],
                                              [Tyxi, Tyyi, Tyzi],
                                              [Tzxi, Tzyi, Tzzi]])
                elif kk == 'Ttense':
                    Txxe = f['tmpxxe'][zsel,ysel,xsel]
                    Tyye = f['tmpyye'][zsel,ysel,xsel]
                    Tzze = f['tmpzze'][zsel,ysel,xsel]
                    Txye = f['tmpxye'][zsel,ysel,xsel]
                    Txze = f['tmpxze'][zsel,ysel,xsel]
                    Tyze = f['tmpyze'][zsel,ysel,xsel]
                    Tyxe = Txye  # these are views; updates to Tyxe also alter Txye
                    Tzxe = Txze
                    Tzye = Tyze
                    out['Ttense'] = np.array([[Txxe, Txye, Txze],
                                              [Tyxe, Tyye, Tyze],
                                              [Tzxe, Tzye, Tzze]])
                elif kk == 'Ttens':
                    Txx = f['tmpxx'][zsel,ysel,xsel]
                    Tyy = f['tmpyy'][zsel,ysel,xsel]
                    Tzz = f['tmpzz'][zsel,ysel,xsel]
                    Txy = f['tmpxy'][zsel,ysel,xsel]
                    Txz = f['tmpxz'][zsel,ysel,xsel]
                    Tyz = f['tmpyz'][zsel,ysel,xsel]
                    Tyx = Txy  # these are views; updates to Tyxe also alter Txye
                    Tzx = Txz
                    Tzy = Tyz
                    out['Ttens'] = np.array([[Txx, Txy, Txz],
                                             [Tyx, Tyy, Tyz],
                                             [Tzx, Tzy, Tzz]])
                elif kk == 'b':
                    bx = f['bx'][zsel,ysel,xsel]
                    by = f['by'][zsel,ysel,xsel]
                    bz = f['bz'][zsel,ysel,xsel]
                    out['b'] = np.array([bx, by, bz])
                elif kk == 'e':
                    ex = f['ex'][zsel,ysel,xsel]
                    ey = f['ey'][zsel,ysel,xsel]
                    ez = f['ez'][zsel,ysel,xsel]
                    out['e'] = np.array([ex, ey, ez])
                elif kk == 'j':
                    jx = f['jx'][zsel,ysel,xsel]
                    jy = f['jy'][zsel,ysel,xsel]
                    jz = f['jz'][zsel,ysel,xsel]
                    # electric current sign is backwards for historical reasons
                    out['j'] = -1 * np.array([jx, jy, jz])
                elif kk == 'v3i':
                    v3xi  = f['v3xi'][zsel,ysel,xsel]
                    v3yi  = f['v3yi'][zsel,ysel,xsel]
                    v3zi  = f['v3zi'][zsel,ysel,xsel]
                    out['v3i'] = np.array([v3xi, v3yi, v3zi])
                elif kk == 'v3e':
                    v3xe  = f['v3xe'][zsel,ysel,xsel]
                    v3ye  = f['v3ye'][zsel,ysel,xsel]
                    v3ze  = f['v3ze'][zsel,ysel,xsel]
                    out['v3e'] = np.array([v3xe, v3ye, v3ze])
                elif kk == 'v4i':
                    v4xi  = f['v4xi'][zsel,ysel,xsel]
                    v4yi  = f['v4yi'][zsel,ysel,xsel]
                    v4zi  = f['v4zi'][zsel,ysel,xsel]
                    gamma = np.sqrt(1 + v4xi**2 + v4yi**2 + v4zi**2)  # c = 1
                    out['v4i'] = np.array([gamma, v4xi, v4yi, v4zi])
                elif kk == 'v4e':
                    v4xe  = f['v4xe'][zsel,ysel,xsel]
                    v4ye  = f['v4ye'][zsel,ysel,xsel]
                    v4ze  = f['v4ze'][zsel,ysel,xsel]
                    gamma = np.sqrt(1 + v4xe**2 + v4ye**2 + v4ze**2)  # c = 1
                    out['v4e'] = np.array([gamma, v4xe, v4ye, v4ze])
                elif kk == 'dense' and 'dense' not in f:
                    out['dense'] = f['dens'][zsel,ysel,xsel] - f['densi'][zsel,ysel,xsel]
                else:
                    # dens, densi, keni, kene,
                    # ex, ey, ez, bx, by, bz, jx, jy, jz
                    # are all covered by this catch-all, though
                    # currents will have flipped sign.
                    out[kk] = f[kk][zsel,ysel,xsel]

        for key in out:
            # Cheaper to transpose vectors, tensors in combined array, versus
            # transposing components one-by-one
            out[key] = np.moveaxis(out[key], [-3,-2,-1], [-1,-2,-3])  # definitely a view
            if squeeze:
                out[key] = np.squeeze(out[key])

        return out


    def spect(self, *want):
        """Unpack and organize TRISTAN spect outputs
        Input: requested outputs...
          Add-ons:
            gamma_ke (replaces gamma, terrible name),
            dNp_dE, dNe_dE, Np, Ne
          Any existing key in spect.* files:
            specp, spece, xsl, dgam
        Output: buncha numpy arrays
        """
        if not want:
            want = ['gamma_ke', 'xsl', 'dgam',
                    'specp', 'spece',
                    'dNp_dE', 'dNe_dE',
                    'Np', 'Ne']

        if 'gamma' in want:
            raise Exception("Use gamma_ke, not gamma")

        out = {}

        fname = path.join(self.outdir, "spect.{:03d}".format(self.n))
        with h5py.File(fname, 'r') as f:
            for key in want:
                if key in f:
                    out[key] = f[key][()]

            if 'gamma_ke' in want:  # each (gamma-1) bin's left bound
                out['gamma_ke'] = f['gamma'][()]

            # Note #1: think of dNp_dE and dNe_dE as:
            #     dN_dE = spec{e,i} * f['gamma'] / (f['gamma']*(10**dgam - 1))
            # Why?
            # * TRISTAN returns spec{e,i} = particle# / f['gamma']
            # * f['gamma']*(10**dgam-1) is each bin's width in non-log
            #   "gamma-space" units.
            #
            # Note #2: for small dgam, 10**dgam - 1 ~= x*ln(10),
            # so dN_dgamma ~= spece/(dgam * ln(10)) is a good approximation.
            if 'dNp_dE' in want:
                specp = f['specp'][()]
                dgam = f['dgam'][()]
                out['dNp_dE'] = specp / (10**dgam - 1)
            if 'dNe_dE' in want:
                spece = f['spece'][()]
                dgam = f['dgam'][()]
                out['dNe_dE'] = spece / (10**dgam - 1)

            if 'Np' in want:
                specp = f['specp'][()]
                xgamma = f['gamma'][()]
                out['Np'] = (specp.T * xgamma).T  # .T's to broadcast correctly
            if 'Ne' in want:
                spece = f['spece'][()]
                xgamma = f['gamma'][()]
                out['Ne'] = (spece.T * xgamma).T  # .T's to broadcast correctly

        return out

    def momentum(self, *want):
        """Unpack and organize TRISTAN momentum outputs
        Input: any existing keys in momentum.* files.  No value-added keywords
        Output: buncha numpy arrays
        """
        if not want:
            want = []
            comps = ['dp{}', 'p{}bin', 'p{}lim', 'p{}elogsp', 'p{}plogsp']
            for i in ['x', 'y', 'z']:
                want += list(map(lambda x: x.format(i), comps))

        out = {}

        fname = path.join(self.outdir, "momentum.{:03d}".format(self.n))
        with h5py.File(fname, 'r') as f:
            for key in want:
                out[key] = f[key][()]

        return out

    def velocity(self, *want):
        """Unpack and organize TRISTAN velocity outputs
        Basically same structure as momentum
        """
        if not want:
            want = []
            comps = ['dv{}', 'v{}bin', 'v{}lim', 'v{}elogsp', 'v{}plogsp']
            for i in ['x', 'y', 'z']:
                want += list(map(lambda x: x.format(i), comps))

        out = {}

        fname = path.join(self.outdir, "velocity.{:03d}".format(self.n))
        with h5py.File(fname, 'r') as f:
            for key in want:
                out[key] = f[key][()]

        return out

    def prtl(self, *want, stem='prtl.tot', fname=None):
        """Unpack and organize TRISTAN prtl outputs
        Input: requested outputs names.  Valid are...
                xi, yi, zi, xe, ye, ze
                ui, vi, wi, ue, ve, we
                proci, indi, proce, inde
                or any other field you may have included in prtl outputs
            kwargs:
                stem - file name prefix, assumed in run output directory
                fname - custom filename, user should specify full path
        Output: buncha numpy arrays
        """
        if not want:
            want = ['xi', 'yi', 'zi', 'xe', 'ye', 'ze',
                    'ui', 'vi', 'wi', 'ue', 've', 'we',
                    'proci', 'indi', 'proce', 'inde']

        out = {}

        if fname is None:
            fname = path.join(self.outdir, "{:s}.{:03d}".format(stem, self.n))
        with h5py.File(fname, 'r') as f:
            for key in want:
                out[key] = f[key][()]

        for key in ['xi', 'yi', 'zi', 'xe', 'ye', 'ze']:
            if key in out:
                out[key] = out[key] - NGHOST - 1  # convert to zero-based index

        return out


class TristanInput(MutableMapping):
    """TRISTAN-MP input file representation"""

    def __init__(self, fname):
        """TRISTAN-MP run outputs"""
        self.fname = fname
        # Requirement: keys for config, line_nums must be identical sets
        if path.isfile(fname):
            self.config, self.lines, self.line_nums = self.read_input(fname,
                                                        with_line_nums=True)
        else:
            self.config = {}
            self.lines = []
            self.line_nums = {}
        assert set(self.config.keys()) == set(self.line_nums.keys())

    # Five abstract methods required for MutableMapping
    # (superclasses: Mapping, Collection, Sized, Iterable, Container)

    def __getitem__ (self, key):
        return self.config[key]

    def __setitem__ (self, key, value):
        self.config[key] = self.type_convert(key, value)

        if key not in self.line_nums:
            line = "{} = {:s}\n".format(key, self.string_format(key, value))
            self.lines.append(line)
            self.line_nums[key] = len(self.lines) - 1

        else:
            line_num = self.line_nums[key]
            line = self.lines[line_num]

            # Preserve comment, try to preserve whitespace alignment
            if '#' in line:
                text, comment = line.split('#', maxsplit=1)
            else:
                text, comment = line, None
            splitter = re.compile(r'^(.*?)(\s*=\s*)(.*)$')
            match = splitter.match(text)
            assert match, "Line# {} missing key/value pair".format(line_num)
            assert match.group(1).strip() == key, "Got wrong key"

            key_str = match.group(1)
            assign_str = match.group(2)
            n = len(match.group(3))  # old value_str

            padfmt = "{{:<{}s}}".format(str(n))
            value_str = padfmt.format(self.string_format(key, value))

            # Perform update
            line = key_str + assign_str + value_str
            if comment:
                if value_str[-1] != ' ':  # used up all the available whitespace
                    line = line + ' #' + comment  # comment carries newline
                else:
                    line = line + '#' + comment  # comment carries newline
            else:
                line += '\n'  # re-attach newline
            self.lines[line_num] = line

        # Finished updating; sanity check keys
        assert set(self.config.keys()) == set(self.line_nums.keys())

    def __delitem__ (self, key):
        line_num = self.line_nums[key]
        del self.config[key]
        del self.lines[line_num]
        del self.line_nums[key]
        for k in self.config:
            if self.line_nums[k] > line_num:
                self.line_nums[k] = self.line_nums[k] - 1

    def __iter__ (self):
        return iter(self.config)

    def __len__ (self):
        return len(self.config)

    # TRISTAN-MP input-specific methods

    def read_input(self, fname, with_line_nums=False):
        """Parse a TRISTAN-MP input file
        Inputs:
            fname (string): path to TRISTAN-MP input file
        Kwargs:
            with_line_nums (Boolean): also return dict mapping key to line
                number.  Most users shouldn't need; for internal class use.
        Output:
            config (dict), lines (list)
        """
        config = {}
        lines = []
        line_nums = {}

        with open(fname, 'r') as f:
            splitter = re.compile(r'^(.*?)=(.*)')
            for line_num, line in enumerate(f):
                text = (line.split('#', maxsplit=1))[0]  # leftmost '#'
                match = splitter.match(text)
                if match:
                    key = match.group(1).strip()
                    value = match.group(2).strip()
                    if key in config:
                        warn("Duplicate key '{}'".format(key)
                             + " in {}".format(fname))
                    config[key] = self.type_convert(key, value)
                    line_nums[key] = line_num
                lines.append(line)

        if with_line_nums:
            return config, lines, line_nums
        return config, lines

    def type_convert(self, key, value):
        """Determine correct Python type for a key's value"""
        # <node_configuration>
        if key in ['sizey', 'sizez']:
            return int(value)
        # <time>
        if key in ['last', 'timespan']:
            return int(value)
        if key in ['c']:
            return float(value)
        # <grid>
        if key in ['mx0', 'my0', 'mz0']:
            return int(value)
        # <algorithm>
        if key in ['ntimes', 'splitparts']:
            return int(value)
        # <restart>
        if key in ['intrestart']:
            return int(value)
        # <output>
        if key in ['interval', 'torqint', 'pltstart', 'istep', 'istep1', 'stride', 'stride1', 'savetmp']:
            return int(value)
        # <boundaries>
        if key in ['periodicx', 'periodicy', 'periodicz']:
            return int(value)
        # <domain>
        # <fields>
        if key in ['btheta', 'bphi']:
            return float(value)
        # <particles>
        if key in ['ppc0', 'maxptl0']:
            return int(float(value))  # warning: this truncates!
        if key in ['sigma', 'delgam', 'mi', 'me', 'gamma0']:
            return float(value)
        if key in ['c_omp']:
            return float(value)
        if key in ['exp_x', 'exp_y', 'exp_z']:  # compress box
            return float(value)
        # <problem>
        if key in ['rRH', 'temperature_ratio', 'hi']:  # box1d setup
            return float(value)
        if key in ['wall', 'rightclean', 'slowinj', 'rstart_jump',
                   'rstep_first', 'rstep_jump']:
            return int(value)
        if key in ['u2z', 'u_sh', 'injfrac', 'wallgam']:
            return float(value)
        # <problem> tristan-mp_expand: extraprt power law distribution
        if key in ['pl_ppc0', 'pl_extraprt', 'pl_pick_species']:
            return int(value)
        if key in ['pl_slope', 'pl_weight', 'pl_pmin_i', 'pl_pmax_i',
                   'pl_pmin_e', 'pl_pmax_e']:
            return float(value)
        # <problem> tristan-mp_xshock: user_wave_pump
        if key in ['wave_thetaBkdeg', 'wave_k', 'wave_omega',
                   'wave_ey_phasedeg', 'wave_ez_phasedeg', 'wave_ey_ex',
                   'wave_ez_ex', 'wave_ex_b0', 'pump_tramp', 'pump_toff']:
            return float(value)
        return value

    def string_format(self, key, value):
        """Formatting for file output, for given key's value; no whitespace padding"""
        # <node_configuration>
        if key in ['sizey', 'sizez']:
            fmttype = "d"
        # <time>
        elif key in ['last', 'timespan']:
            fmttype = "d"
        elif key in ['c']:
            fmttype = "f"
        # <grid>
        elif key in ['mx0', 'my0', 'mz0']:
            fmttype = "d"
        # <algorithm>
        elif key in ['ntimes', 'splitparts']:
            fmttype = "d"
        # <restart>
        elif key in ['intrestart']:
            fmttype = "d"
        # <output>
        elif key in ['interval', 'torqint', 'pltstart', 'istep', 'istep1', 'stride', 'stride1', 'savetmp']:
            fmttype = "d"
        # <boundaries>
        # <domain>
        # <fields>
        elif key in ['btheta', 'bphi']:
            fmttype = "f"
        # <particles>
        elif key in ['sigma', 'delgam', 'gamma0', 'exp_x', 'exp_y', 'exp_z']:
            fmttype = ".6E"
        elif key in ['c_omp']:  # usually int in practice, but can be float
            fmttype = "g"
        elif key in ['ppc0', 'maxptl0']:
            fmttype = "G"  # these should be ints, but can be large (e.g. 1e9)
        elif key in ['mi', 'me']:
            fmttype = "f"
        # <problem>
        elif key in ['temperature_ratio', 'rRH', 'hi', 'he']:  # box1d
            fmttype = ".5f"
        elif key in ['user_part_bcs', 'wall', 'rightclean', 'slowinj',
                     'rstart_jump', 'rstep_first', 'rstep_jump']:  # shock
            fmttype = "d"
        elif key in ['wallgam', 'u_sh', 'u2z', 'injfrac', 'rightclean', 'slowinj']:  # shock
            fmttype = "f"
        # <problem> tristan-mp_expand: extraprt power law distribution
        elif key in ['pl_ppc0']:
            fmttype = "G"  # should be int, but can be large (e.g. 1e9)
        elif key in ['pl_extraprt', 'pl_pick_species']:
            fmttype = "d"  # should be only 0 or 1.
        elif key in ['pl_slope', 'pl_weight', 'pl_pmin_i', 'pl_pmax_i',
                     'pl_pmin_e', 'pl_pmax_e']:
            fmttype = "f"
        # <problem> tristan-mp_expand: frozen particles or isotropic compression
        elif key in ['freezeion', 'freezelec', 'isoion', 'isolec']:
            fmttype = "d"  # should be only 0 or 1.
        elif key in ['exp_iso']:
            fmttype = ".6E"
        # <problem> tristan-mp_xshock: user_wave_pump
        elif key in ['wave_thetaBkdeg', 'wave_ey_phasedeg',
                     'wave_ez_phasedeg', 'pump_tramp', 'pump_toff']:
            fmttype = ".6f"
        elif key in ['wave_k', 'wave_omega', 'wave_ey_ex', 'wave_ez_ex',
                     'wave_ex_b0',]:
            fmttype = ".6E"
        else:
            warn("Unrecognized key {}, formatting as float".format(key))
            fmttype = "f"
        fmtstr = "{{:{}}}".format(fmttype)
        return fmtstr.format(value)

    def write(self, out, clobber=False):
        if not clobber and path.exists(out):
            raise IOError("File exists, not clobbering: {}".format(out))
        else:
            with open(out, 'w') as f:
                f.writelines(self.lines)
                f.flush()
                os.fsync(f)

    def show(self):
        for line in self.lines:
            print(line, end='')


class TristanParam(Mapping):
    """TRISTAN-MP param file representation.  Does lazy memoizing"""

    def __init__(self, fname):
        """TRISTAN-MP param file representation.  Does lazy memoizing"""
        self._fname = fname
        self._param = {}

    def load(self):
        self._param = {}
        with h5py.File(self._fname, 'r') as f:
            for key in f:
                val = f[key][()]
                if val.dtype == 'float32':
                    val = val.astype('float64')
                elif val.dtype == 'int32':
                    val = val.astype('int64')
                self._param[key] = val[0]

    def __getitem__(self, key):
        if not self._param:
            self.load()
        return self._param[key]

    def __iter__ (self):
        if not self._param:
            self.load()
        return iter(self._param)

    def __len__ (self):
        if not self._param:
            self.load()
        return len(self._param)


# Utilities to work with TristanRun, Scene, TristanInput

def prtlindex(proc, ind, prtl):
    """Get index of (proc,ind)-tagged particle in prtl.tot.(xxx) outputs

    TRISTAN represents proc,ind as ints, but outputs them as floats.

    Input: proc (int), ind (int), prtl (dict of outputs from Scene.prtl(...)
        prtl dict must contain 'inde', 'proce', 'indi', 'proci'
    Output:
        index (int) of tagged particle
    """
    # Sequential np.where(...) is 35% faster (56 +/- 0.07 vs. 87 +/- 0.13 microseconds)
    # than: np.where(np.logical_and(prtl['proce'] == proc, prtl['inde'] == ind))
    if ind % 2 == 0:  # electrons, even indices
        hits = np.where(prtl['inde'] == ind)[0]
        hits2 = np.where(prtl['proce'][hits] == proc)[0]
        lookup = hits[hits2]
    else:  # ions, odd indices
        hits = np.where(prtl['indi'] == ind)[0]
        hits2 = np.where(prtl['proci'][hits] == proc)[0]
        lookup = hits[hits2]
    if lookup:
        assert len(lookup) == 1
        return lookup[0]


def prtlindices(procs, inds, prtl, allowmiss=False, allowevenodd=False, species='unknown'):
    """Find multiple procs, inds in given prtl file

    BEWARE: NO COLLISIONS ALLOWED; (proc,ind) pairs must ALL BE UNIQUE
    OR ELSE THINGS BREAK HORRIBLY

    Input:
        procs = np.array of proc#s
        inds = np.array of indices
        prtl = TristanScene.prtl() dict containing proce, inde, proci, indi

        allowevenodd = permit indices to have both even odd,
            might occur if e.g. simulation has uneven number of prtls...

    Output:
        list of indices, that you can use to index into prtl to grab particles tagged by (procs,inds)

    Sentinel value: index of -1  should NEVER occur

    Code is moderately speed optimized.
    Various tweaks cut run time from ~2.5 to 1.1 sec,
    at expense of lengthier/uglier code.

    NOTES ON SPEED FOLLOW.
    Tested on "mi49Ms7betap0.25theta0_2d-llb_gamma1.67_dgamr0.5_my144_hi-res".

    Sorting takes just 0.229 msec.  This is O(n*log(n)).
    Next part is O(n) but takes much longer.

        In principle we could use searchsorted-like algorithm to do binary search on each key.
        This is O(#search * log(#prtl,all)), vs. O(#prtl,all)

        # Number of lookups for simple linear iteration
        print( len(prtl['inde']) )

        # Number of lookups for _independent_ binary search of each key...
        # I would hope that numpy has implemented something more efficient.
        # Even if we get log(n) here, we are still bottlenecked by n*log(n) sort performance (in practice that's actually not true, sort is very efficient)
        print( np.log2(len(prtl['inde'])) * len(keyfind) )

        It turns out that these numbers are comparable, for our use case.  So don't sweat.

    Time to load prtl from file, first time (use `%%timeit -n1 -r1`): 5.66s, 7.88s (clocked on n=200,201)
    Time to load prtl, after having cached: ~55 msec +/- few
    Time to lexsort prtl proc and ind: ~ 220 msec
    Time to search for ~87000 (proc,ind) in ~1.5 million prtl (proc,ind): ~1s

    Total run time is ~1.3 sec. This is acceptable, in real use case, time will be dominated by prtl file read (~5-6 sec) and plotting (~1-5+? sec).
    """
    assert len(inds) == len(procs)

    if len(inds) == 0:
        # empty numpy array dtype defaults to float64, which causes:
        # "IndexError: arrays used as indices must be of integer (or boolean) type"
        # therefore, manually set dtype to avoid error.
        return np.array([], dtype='int64')

    if allowevenodd and species == 'e':
        procs_all = prtl['proce']
        inds_all = prtl['inde']
    elif allowevenodd and species == 'i':
        procs_all = prtl['proci']
        inds_all = prtl['indi']
    elif allowevenodd:
        raise Exception("Tell me what you want what you really really want")

    elif not np.any(inds % 2):  # electrons, all indices%2 == 0
        procs_all = prtl['proce']
        inds_all = prtl['inde']
    elif np.all(inds % 2):  # ions, all indices%2 == 1
        procs_all = prtl['proci']
        inds_all = prtl['indi']
    else:
        raise Exception("Indices has both even and odd values; cannot mix e- and ion indices")

    arrayinds = -1 * np.ones_like(procs, dtype=int)

    # sort lists of match (selected, to-search-for) prtls and all candidate prtls
    keysrch = np.lexsort((inds, procs))
    keyall = np.lexsort((inds_all, procs_all))  # procs must be primary search key

    if not allowmiss:
        # this algorithm ONLY works if we are guaranteed to find
        # all requested prtl; i.e., search is subset of candidates

        i_srt = 0
        i = keysrch[i_srt]
        procsrch = procs[i]
        indsrch = inds[i]
        imax = len(keysrch)  # max for both i and i_srt, so no ambiguity

        #j_srt = 0
        #j = keyall[j_srt]
        #proccand = prtl['proce'][j]
        #indcand = prtl['inde'][j]
        jmax = len(keyall)  # max for both j and j_srt, so no ambiguity

        for j_srt in range(jmax):
            j = keyall[j_srt]
            proccand = procs_all[j]
            indcand = inds_all[j]

            if procsrch == proccand and indsrch == indcand:
                arrayinds[i] = j  # map input (proc,ind) index to big prtl array index
                if i_srt == imax - 1:
                    break
                i_srt += 1
                i = keysrch[i_srt]
                procsrch = procs[i]
                indsrch = inds[i]

        assert -1 not in arrayinds  # all search prtl must be found

    else:
        # this algorithm works even if some search prtls are
        # missing from candidate list

        i_srt = 0
        i = keysrch[i_srt]
        procsrch = procs[i]
        indsrch = inds[i]

        j_srt = 0
        j = keyall[j_srt]
        proccand = procs_all[j]
        indcand = inds_all[j]

        imax = len(keysrch)  # max for both i and i_srt, so no ambiguity
        jmax = len(keyall)  # max for both j and j_srt, so no ambiguity

        while i_srt < imax and j_srt < jmax:

            if procsrch == proccand and indsrch == indcand:
                # hit, map input (proc,ind) index to big prtl array index
                arrayinds[i] = j

                i_srt += 1
                if i_srt == imax - 1:
                    break
                i = keysrch[i_srt]
                procsrch = procs[i]
                indsrch = inds[i]

                j_srt += 1
                if j_srt == jmax - 1:
                    break
                j = keyall[j_srt]
                proccand = procs_all[j]
                indcand = inds_all[j]

            elif procsrch > proccand or (procsrch == proccand and indsrch > indcand):
                # miss
                j_srt += 1
                if j_srt == jmax - 1:
                    break
                j = keyall[j_srt]
                proccand = procs_all[j]
                indcand = inds_all[j]

            elif procsrch < proccand or (procsrch == proccand and indsrch < indcand):
                # miss
                i_srt += 1
                if i_srt == imax - 1:
                    break
                i = keysrch[i_srt]
                procsrch = procs[i]
                indsrch = inds[i]

            else:
                # this should never happen
                raise Exception("Error: code bug, proc/ind case logic failed")

        # only return inds for found prtls.
        arrayinds = arrayinds[arrayinds != -1]

        # user may want to know which prtl were found or not found.
        # Below code accomplishes that, but rather inelegantly
        # because user must filter on arrayinds == len(prtl['proc{e,i}']).
        # I have no immediate need for this behavior, so skip.
        #arrayinds[arrayinds == -1] = jmax+1 # if prtl not found, an attempt to index should break

    return arrayinds


def prtlselxyz(prtl, xsel=None, ysel=None, zsel=None, species='e'):
    """Select particles from intervals in x,y,z
    If no interval specified, assumed all x,y,z-coordinate OK
    (e.g., specifying xsel gives you all prtl in a slab of all y,z and finite x)

    Returns procs, inds
    """

    if xsel is None:
        xsel = (-np.inf, +np.inf)
    if ysel is None:
        ysel = (-np.inf, +np.inf)
    if zsel is None:
        zsel = (-np.inf, +np.inf)

    if species == 'e':
        #prtl = s.prtl('proce', 'inde', 'xe', 'ye', 'ze')
        proc, ind = prtl['proce'], prtl['inde']
        x, y, z = prtl['xe'], prtl['ye'], prtl['ze']
    elif species == 'i':
        #prtl = s.prtl('proci', 'indi', 'xi', 'yi', 'zi')
        proc, ind = prtl['proci'], prtl['indi']
        x, y, z = prtl['xi'], prtl['yi'], prtl['zi']
    else:
        raise Exception("Invalid species '{:s}', must be 'e' or 'i'".format(species))

    sel = np.logical_and.reduce(
                (x > xsel[0], x < xsel[1],
                 y > ysel[0], y < ysel[1],
                 z > zsel[0], z < zsel[1],)
    )

    return proc[sel], ind[sel]


def prtltraj(pos, cdt=None, paxis=None, my0=None,
             ax=None, marker=None, **kwargs):
    """
    PLOT a "de-aliased" particle trajectory in domain with one periodic dimension,
    given particle positions at evenly sampled times.

    De-aliasing method: pick the closest alias.  This is guaranteed correct if cdt < my0/2.

    Notes:
    1. when cdt > my0/2 (large time sampling interval), some particle position aliases cannot be disambiguated.
       A warning is given.
    2. we don't use velocities to help resolve particle aliases.  Why?
       - because of how 2D vector components project, we need to sample ~1/8th e- gyroperiod or less
         to confidently use velocities to help resolve aliasing.
         otherwise, velocities can easily point opposite w.r.t. next position and tell us nothing.
       - scattering is fast (< electron gyrotime)
    Best to only trace prtl trajectories in runs w/ sufficient time resolution.

    Input:
        pos = 2D numpy array of points, shape is (#pts, 2)
            for TRISTAN-MP: these must be in code-space units with no ghost cells
        cdt = maximum distance prtl can move between time steps
        paxis = axis of periodic dimension
        my0 = length of periodic dimension (from periodic wall to periodic wall)
        ax = matplotlib.axes.Axes instance; if None, a new figure and axes will be created
        marker = marker for trajectory, passed to pyplot.plot(...)
        **kwargs = plot kwargs, passed to pyplot.plot(...)

    Return: list of matplotlib line objects added
    """
    assert len(pos.shape) == 2 and pos.shape[1] == 2
    assert cdt is not None
    assert paxis is not None
    assert my0 is not None
    if cdt > my0/2:
        warn("Prtl tracks may be incorrect, c-speed prtl can traverse >50% box width in one interval, aliasing reconstruction ambiguous")

    if ax is None:
        plt.figure()
        ax = plt.gca()
    plt.sca(ax)

    mpl_lines = []

    trajectory = [pos[0]]

    # How do we draw trajectory to next (i-th) particle?
    for i in range(1, len(pos)):

        # particles cannot be >1 box width apart; this guards against over-eager/bad de-aliasing
        assert abs(pos[i][paxis] - pos[i-1][paxis]) < my0

        # If particle can't have crossed boundary going from pos[i-1] to pos[i],
        # no need for detailed analysis
        if pos[i][paxis] > cdt and (my0 - pos[i][paxis]) > cdt:
            trajectory.append(pos[i])
            continue

        distances = [pos[i][paxis]-pos[i-1][paxis] - my0,
                     pos[i][paxis]-pos[i-1][paxis],
                     pos[i][paxis]-pos[i-1][paxis] + my0]
        # argmin->0 means n=-1 alias, argmin->1 means n=0 alias, etc...
        order = np.argmin(np.abs(distances)) - 1

        if order == 0:
            trajectory.append(pos[i])
        else:
            # attach "ldeal" (left dealias) to current trajectory piece,
            # attach "rdeal" (right dealias) to next trajectory piece
            pos_ldeal = [pos[i][0], pos[i][1]]  # explicitly copy
            pos_ldeal[paxis] += order * my0
            pos_rdeal = [pos[i-1][0], pos[i-1][1]]
            pos_rdeal[paxis] -= order * my0  # change sign

            trajectory.append(pos_ldeal)

            # Always passes >=2 points, never <=1, to prtltraj by construction
            _ = prtltraj(np.concatenate( ([pos_rdeal], pos[i:]) ),
                         cdt=cdt, paxis=paxis, my0=my0, ax=ax, marker=marker,
                         **kwargs)
            mpl_lines.extend(_)
            break

    trajectory = np.array(trajectory)

    # Edge case (2018 Jul 26): if traj has two points, we plot
    # * ghost dealias piece (no markers, border crossing)
    # * single point within domain
    # If caller sets marker='None', single point is an invisible mpl.Artist.
    # That's OK.  It should be present in general case (user wants markers on
    # trajectory)
    #
    # if 1 point, must be user call, cannot be recursive call.
    #   point should not lie outside [0, my0]
    # if 2 points, should only have one ghost dealias piece
    #   (i.e., at least one of two points should be within [0, my0]...)
    # if >=3 points possible to have two ghost dealias pieces

#    # Don't show marker on "ghost dealias" pieces of trajectory
#    if trajectory[0][paxis] < 0 or trajectory[0][paxis] > my0:
#        _ = plt.plot(*(trajectory[0:2].T), marker="None", **kwargs)
#        mpl_lines.extend(_)  # return "ghost pieces" to caller, too
#        trajectory = trajectory[1:]
#    if trajectory[-1][paxis] < 0 or trajectory[-1][paxis] > my0:
#        _ = plt.plot(*(trajectory[-2:].T), marker="None", **kwargs)
#        mpl_lines.extend(_)  # return "ghost pieces" to caller, too
#        trajectory = trajectory[:-1]
#
#    _ = plt.plot(*(trajectory.T), marker=marker, **kwargs)  # overly explicit for clarity
#    mpl_lines.extend(_)
#    return mpl_lines

    # OR simply accept markers on trajectory edges.  No one should see them in
    # practice, and I often don't plot markers on trajectories.
    # This makes the code + plotting simpler.
    #
    # suppressing markers on "ghost dealias pieces" was useful for debugging,
    # so keep old code around for now

    _ = plt.plot(*(trajectory.T), marker=marker, **kwargs)  # overly explicit for clarity
    mpl_lines.extend(_)
    return mpl_lines


def prtltraj_pts(pos, cdt=None, paxis=None, my0=None):
    """
    get points of prtl trajectory, but dealias in a "continuous" way

    Input:
        pos = 2D numpy array of points, shape is (#pts, 2)
            for TRISTAN-MP: these must be in code-space units with no ghost cells
        cdt = maximum distance prtl can move between time steps
        paxis = axis of periodic dimension
        my0 = length of periodic dimension (from periodic wall to periodic wall)
    """
    assert len(pos.shape) == 2 and pos.shape[1] == 2
    assert cdt is not None
    assert paxis is not None
    assert my0 is not None
    if cdt > my0/2:
        warn("Prtl tracks may be incorrect, c-speed prtl can traverse >50% box width in one interval, aliasing reconstruction ambiguous")

    trajectory = pos.copy()

    # track of which tile the particle is currently in, and apply offset
    # as we go to make a continuous curve.
    # ntile accumulates over time...
    ntile = 0

    for i in range(1, len(pos)):
        distances = [pos[i][paxis]-pos[i-1][paxis] - my0,
                     pos[i][paxis]-pos[i-1][paxis],
                     pos[i][paxis]-pos[i-1][paxis] + my0]
        # argmin->0 means n=-1 alias, argmin->1 means n=0 alias, etc...
        # order = -1 means we moved one tile down,
        # order = 0 means no change,
        # order = +1 means we moved one tile up
        order = np.argmin(np.abs(distances)) - 1
        ntile += order
        trajectory[i][paxis] += ntile*my0

    return trajectory


if __name__ == '__main__':
    pass

