#!/usr/bin/env python3

from __future__ import division, print_function


from datetime import datetime
import numpy as np
import numpy.fft
from numpy.fft import fftshift, fftfreq
import os.path as path
import scipy
import scipy.signal

#from mark import TristanRun, MkShock
from myutil import printmem


def one_sided_psd(f, arr, axis=None):
    """
    Given 1-dim array of strictly-ascending FFT frequencies f, and n-dim array of FFT power,
    sum the FFT power at positive and negative frequencies along user-requested axis
    so as to obtain a single-sided wave spectrum.
    Normalization of input and output spectra is kept the same.
    For len(f) even, output spectrum has shape len(f)/2 + 1.
    """
    assert axis is not None  # a required kwarg

    assert len(f) == arr.shape[axis]

    if len(f) % 2 == 0:
        # even number of FFT points
        assert np.all(np.diff(f) > 0)  # strictly ascending
        assert np.abs(f[0]) > np.abs(f[-1])
        assert np.abs(f[1]) == f[-1]

        nmid = round(len(f)/2)
        assert f[nmid] == 0.

        # positive, non-zero FFT frequencies, forward slice
        ps = np.r_[nmid+1:len(f)]
        # negative, non-zero FFT frequencies, forward slice
        # excluding the most-negative freq, which has no corresponding +ve value
        ns = np.r_[1:nmid]
        ns = ns[::-1]
        # zero FFT frequency
        zs = np.r_[nmid:nmid+1]
        # most-negative FFT frequency, which has no corresponding +ve value
        # "es" stands for "edge slice"
        es = np.r_[0:1]

        # make sure that slices line up exactly
        np.all(np.equal(f[ps], np.abs(f[ns])))

        # when adding the power together, and trying to compare the energy
        # at each frequency... do I need to double the power at freq=0?
        # what about the power at "most-negative" freq?
        # answer: no, b/c the sum of power has to satisfy parseval . . .
        power_pos = np.take(arr, ps, axis=axis)
        power_neg = np.take(arr, ns, axis=axis)
        power_zero = np.take(arr, zs, axis=axis)
        power_edge = np.take(arr, es, axis=axis)

        power_single_sided = np.concatenate( (power_zero,power_pos+power_neg,power_edge), axis=axis)

    else:
        raise Exception("not implemented")

    return power_single_sided


class TristanFldFFT(object):
    """
    Tristan field fourier transform, with useful defaults
    for windowing, coordinates, and fftshift frequencies

    Philosophy -- TristanFldFFT object holds lots of stuff as simple attributes
    (dot notation), which get auto-populated when you "load" a field
    or perform a fourier transform.

    This is meant to be a very lightweight wrapper around numpy fft with
    convenient defaults.

    Usage:

        r = TristanRun(...)
        s = r[-1]
        flds = s.flds('b')

        bxfft = TristanFldFFT(r, fld=flds['b'][0])
        bxfft.transform(
            twindow=scipy.signal.windows.blackmanharris,
            verbose=True,
        )

        plt.imshow(bxfft.power_omkx.T, origin='lower')
        plt.show()
    """

    def __init__(self, r, fld=None, **kwargs):
        """
        Tristan field fourier transform, with useful defaults
        for windowing, coordinates, and fftshift frequencies
        fld must have shape: (t,x,y,z)
        Default spacing is assumd to be run 'interval' and 'istep',
        but user can change that if they wish
        (kwargs passed to TristanFldFFT.set_fld(...))
        """
        self.run = r
        if fld is not None:
            self.set_fld(fld, **kwargs)

    def clear_fld(self):
        """Clean up, hope that python garbage collection is good enough"""

        self.fld = None

        self.tsamp = None
        self.dsamp = None
        self.omega_conv = None
        self.space_conv = None

        self.om = None
        self.kx = None
        self.ky = None
        self.kz = None

        self.om_bnd = (None, None)
        self.kx_bnd = (None, None)
        self.ky_bnd = (None, None)
        self.kz_bnd = (None, None)

        self.wwtt = None
        self.wwtx = None
        self.wwty = None
        self.wwtz = None

        #self.wfld = None
        self.ifld = None
        self.power_omkx = None
        self.power_omky = None
        self.power_omkz = None
        self.power_kxky = None
        self.power_kxkz = None
        self.power_kykz = None

    def set_fld(self, fld, tsamp=None, dsamp=None, omega_conv=None, space_conv=None):
        """
        fld must have shape: (t,x,y,z)
        Default spacing is assumd to be run 'interval' and 'istep',
        but user can change that if they wish
        """

        self.clear_fld()

        assert fld.ndim == 4
        self.fld = fld
        # choose temporal grid size
        if tsamp is None:
            self.tsamp = self.run.input['interval']
        else:
            self.tsamp = tsamp
        # choose spatial grid size
        if dsamp is None:
            self.dsamp = self.run.input['istep']
        else:
            self.dsamp = dsamp
        # choose default (angular) frequency unit
        if omega_conv is None:
            self.omega_conv = self.run.input['c']/self.run.input['c_omp']
        else:
            self.omega_conv = omega_conv
        # choose default spatial unit
        if space_conv is None:
            self.space_conv = self.run.input['c_omp']
        else:
            self.space_conv = space_conv

        # for traveling wave exp(i*k*x - i*omega*t),
        # expect omega/k sign to match direction of propagation.
        # because numpy fftn doesn't care about time vs space,
        # i.e., maps exp(i*k*x - i*omega*t) -> (+k, -omega)
        # so, manually flip the time frequencies to match intuition
        # for traveling waves.
        # this reversal needs to match behavior in self.transform(...)
        delta = self.tsamp * self.omega_conv
        om_raw = 2*np.pi*fftshift(fftfreq(self.fld.shape[0], d=delta))
        self.om = -1*om_raw[::-1]

        # wavenumber spacing is simpler
        delta = self.dsamp / self.space_conv
        self.kx = 2*np.pi*fftshift(fftfreq(self.fld.shape[1], d=delta))
        self.ky = 2*np.pi*fftshift(fftfreq(self.fld.shape[2], d=delta))
        self.kz = 2*np.pi*fftshift(fftfreq(self.fld.shape[3], d=delta))

        # construct bin edge bounds, useful for pyplot.imshow(...) extent
        # https://matplotlib.org/stable/tutorials/intermediate/imshow_extent.html
        self.om_bnd = (None, None)
        self.kx_bnd = (None, None)
        self.ky_bnd = (None, None)
        self.kz_bnd = (None, None)
        if self.om.size > 1:
            dom = np.diff(self.om)[0]
            self.om_bnd = (self.om[0]-dom/2., self.om[-1]+dom/2.)
        if self.kx.size > 1:
            dkx = np.diff(self.kx)[0]
            self.kx_bnd = (self.kx[0]-dkx/2., self.kx[-1]+dkx/2.)
        if self.ky.size > 1:
            dky = np.diff(self.ky)[0]
            self.ky_bnd = (self.ky[0]-dky/2., self.ky[-1]+dky/2.)
        if self.kz.size > 1:
            dkz = np.diff(self.kz)[0]
            self.kz_bnd = (self.kz[0]-dkz/2., self.kz[-1]+dkz/2.)

        # reset everything
        #self.wfld = None
        self.ifld = None
        self.power_omkx = None
        self.power_omky = None
        self.power_omkz = None
        self.power_kxky = None
        self.power_kxkz = None
        self.power_kykz = None

    def transform(self, twindow=None, xwindow=None, ywindow=None, zwindow=None, fld0=0.0, verbose=False):
        """
        Perform fourier transform and save the result into useful arrays.
        Fourier transform sgn is -ve for time coordinate, +ve for space
        coordinates, so that traveling waves omega/k > 0 are going in the
        "correct" direction.
        """

        if verbose:
            started = datetime.now()
            print("fft starting")

        # Window weighting factor is mean of squared coefficients,
        # <{w_i}^2> = 1/N * sum_{i=0}^{N-1} {w_i}^2
        # For no window (i.e., boxcar), reduces to 1.
        # Expose weighting factors so that user may construct any desired
        # normalizations for transformed fields
        self.wwtt = 1.
        self.wwtx = 1.
        self.wwty = 1.
        self.wwtz = 1.

        myfld = self.fld
        if twindow is not None:
            w = twindow(myfld.shape[0])
            self.wwtt = np.mean(w**2)  # must precede broadcast
            w = w[:, np.newaxis,np.newaxis,np.newaxis]  # be explicit about how we broadcast
            myfld = w*(myfld-fld0) + fld0
        if xwindow is not None:
            w = xwindow(myfld.shape[1])
            self.wwtx = np.mean(w**2)  # must precede broadcast
            w = w[np.newaxis, :, np.newaxis,np.newaxis]  # be explicit about how we broadcast
            myfld = w*(myfld-fld0) + fld0
        if ywindow is not None:
            w = ywindow(myfld.shape[2])
            self.wwty = np.mean(w**2)  # must precede broadcast
            w = w[np.newaxis,np.newaxis, :, np.newaxis]  # be explicit about how we broadcast
            myfld = w*(myfld-fld0) + fld0
        if zwindow is not None:
            w = zwindow(myfld.shape[3])
            self.wwtz = np.mean(w**2)  # must precede broadcast
            w = w[np.newaxis,np.newaxis,np.newaxis,:]  # be explicit about how we broadcast
            myfld = w*(myfld-fld0) + fld0

        if verbose:
            print("fft windows done, elapsed", datetime.now() - started)

        #self.wfld = myfld

        self.ifld = fftshift(np.fft.fftn(myfld))

        # try to conserve memory
        # (not sure if python garbage collector does this promptly)
        del myfld

        # for traveling wave exp(i*k*x - i*omega*t),
        # expect omega/k sign to match direction of propagation.
        # because numpy fftn doesn't care about time vs space,
        # i.e., maps exp(i*k*x - i*omega*t) -> (+k, -omega)
        # so, manually flip the time frequencies to match intuition
        # for traveling waves.
        # this reversal needs to match behavior in self.om(...)
        self.ifld = self.ifld[::-1,...]  # flip must come after fftshift

        if verbose:
            print("fft transform done, elapsed", datetime.now() - started)

        # normalize so that mean(power) = mean(fld**2) / product(fld.shape)
        # is an estimator of the total energy per unit spacetime volume, which
        # can be meaningfully compared between simulations regardless of
        # spacetime domain size, sampling, and windowing.
        power = np.abs(self.ifld)**2 / np.prod(self.fld.shape)
        power *= 1./(self.wwtt*self.wwtx*self.wwty*self.wwtz)

        self.power_omkx = np.mean(power, axis=(2,3))
        self.power_omky = np.mean(power, axis=(1,3))
        self.power_omkz = np.mean(power, axis=(1,2))
        self.power_kxky = np.mean(power, axis=(0,3))
        self.power_kxkz = np.mean(power, axis=(0,2))
        self.power_kykz = np.mean(power, axis=(0,1))

        # try to conserve memory
        # (not sure if python garbage collector does this promptly)
        del power

        if verbose:
            print("fft 2D mean slices done, elapsed", datetime.now() - started)
            print("fft all done")

    def spectrogram_homebrew(self, iibin, iispacing, twindow=None, xwindow=None,
                             ywindow=None, zwindow=None, fld0=0.0):
        """
        Borrowing some design from
        https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.spectrogram.html
        but my coordinate axis convention is flipped, and I also change the density convention...
        """
        # TODO maybe get rid of fld0 by implementing a simpler "constant" detrending for the window function?
        # not critical.
        # -ATr,2022jan19

        # ---------------------------
        # spectrogram coordinate axes

        # location of time bin centers (not edges!!)
        # for iibin=1, this corresponds to exact time of output snapshot
        # for iibin=2, this corresponds to time halfway between each snapshot
        # et cetera for larger iibin.
        t_slice = (iibin-1)/2. + np.arange(0, self.fld.shape[0]-iibin, iispacing)  # in tristan units...
        t_slice *= self.tsamp

        # manually flip frequencies to make k,omega > 0
        # correspond to a forward traveling wave, because
        # numpy fftn would map exp(i*k*x - i*omega*t) -> (+k, -omega) without flip
        om_slice = 2*np.pi*fftshift(fftfreq(iibin, d=self.tsamp*self.omega_conv))
        # below -1 x list reversal operation is not redundant.
        # For an even number of sample points, it shifts nyquist frequency position.
        om_slice = -1*om_slice[::-1]

        # ---------------------
        # prepare field for FFT

        assert self.fld.ndim == 4  # shape is (t,x,y,z)
        myfld = self.fld

        # Window weighting factor is mean of squared coefficients,
        # <{w_i}^2> = 1/N * sum_{i=0}^{N-1} {w_i}^2
        # For no window (i.e., boxcar), reduces to 1.
        wwtt = 1.
        wwtx = 1.
        wwty = 1.
        wwtz = 1.

        # Apply any user-requested spatial windows
        if xwindow is not None:
            w = xwindow(myfld.shape[1])
            wwtx = np.mean(w**2)  # must precede broadcast
            w = w[np.newaxis, :, np.newaxis,np.newaxis]  # be explicit about how we broadcast
            myfld = w*(myfld-fld0) + fld0
        if ywindow is not None:
            w = ywindow(myfld.shape[2])
            wwty = np.mean(w**2)  # must precede broadcast
            w = w[np.newaxis,np.newaxis, :, np.newaxis]  # be explicit about how we broadcast
            myfld = w*(myfld-fld0) + fld0
        if zwindow is not None:
            w = zwindow(myfld.shape[3])
            wwtz = np.mean(w**2)  # must precede broadcast
            w = w[np.newaxis,np.newaxis,np.newaxis,:]  # be explicit about how we broadcast
            myfld = w*(myfld-fld0) + fld0

        # Pre-allocate the output spectrogram with shape (slices, omega, x, y, z)
        Sxx = np.empty(
            (
                len(range(0, self.fld.shape[0]-iibin, iispacing)),  # number of slices along t
                iibin,  # omega axis
                *self.fld.shape[1:]  # (x,y,z) axes
            ),
        )

        # -------------------------------
        # compute FFT for each time slice

        for iienum, ii in enumerate(range(0, self.fld.shape[0]-iibin, iispacing)):

            myfld_slice = myfld[ii:ii+iibin]

            if twindow is not None:
                w = twindow(myfld_slice.shape[0])
                wwtt = np.mean(w**2)  # must precede broadcast
                w = w[:, np.newaxis,np.newaxis,np.newaxis]  # be explicit about how we broadcast
                myfld_slice = w*(myfld_slice-fld0) + fld0

            ifld_slice = fftshift(np.fft.fftn(myfld_slice))
            # manually flip time frequencies to make k,omega > 0
            # correspond to a forward traveling wave, because
            # numpy fftn would map exp(i*k*x - i*omega*t) -> (+k, -omega)
            # without below flip applied.
            ifld_slice = ifld_slice[::-1,...]  # flip must come after fftshift

            Sxx_slice = np.abs(ifld_slice)**2
            # re-scale to obtain a density function in n-dimensional fourier space
            # that is properly scale- and sampling-invariant.
            Sxx_slice *= 1./np.prod(myfld_slice.shape)
            Sxx_slice *= 1./(wwtt*wwtx*wwty*wwtz)

            Sxx[iienum,...] = Sxx_slice

        return t_slice, om_slice, Sxx

    def spectrogram_scipy(self, **kwargs):
        """
        Wrapper layer around
        https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.spectrogram.html
        which I expect to be faster / more efficient than any home-brewed scheme that I can create.
        """
        f, t, Sxx = scipy.signal.spectrogram(
            self.fld.T,  # scipy coordinate convention is flipped w.r.t my own
            fs = 1.,  # for some reason, this modifies the power spectral density scaling... which is NOT what I want
            return_onesided=False,
            **kwargs
        )

        # fix to match my sign convention for traveling waves,
        f = fftshift(f)
        f = -1*f[::-1]
        # and rescale from range [-0.5,+0.5] to angular units scaled by omega,p(t=0)
        f *= 1./self.tsamp * 2*np.pi/self.omega_conv

        # rescale to correct time
        t *= self.tsamp

        # default output shape is (z,y,x,frequency,tslice)
        # shift axes back to my personal convention of (tslice,frequency,x,y,z)
        Sxx = fftshift(Sxx, axes=-2)
        Sxx = Sxx[...,::-1,:]
        Sxx = Sxx.T

        return t, f, Sxx


if __name__ == '__main__':
    pass
