README
======

`mark` is a wrapper layer for reading TRISTAN input and output files into numpy
arrays.

Reading output files (flds, prtls) into Python memory should work, perhaps with
minor code edits, for most any version of TRISTAN and most any TRISTAN user
file.
Many bits of code are designed for specific use cases (i.e., Aaron Tran's
work), and not everything is guaranteed to work.
Some code and design principles have been adopted, implicitly, from analysis
code by Xinyi Guo.

Not tested with [Tristan-v2](https://princetonuniversity.github.io/tristan-v2/).

Python 3.6 or higher is required.


Example usage
-------------

Load a TRISTAN run and read fields into numpy arrays.

	from mark import TristanRun
	run = TristanRun("/path/to/your/rundir")
	for scene in run:
		c_omp = scene.param('c_omp')  # param.xxx quantities
		dimf = scene.dimf()  # dimensions of istep-downsampled flds.tot arrays
		flds = scene.flds('densi', 'v3i', 'j')  # dict of flds.tot arrays
		v3i = flds['v3i']        # numpy array of shape (3, dimf[0], dimf[1], dimf[2])
		# do stuff . . .


Manifest
--------

* Object-like interface to TRISTAN runs and files:

        mark.TristanRun
        mark.Scene
        mark.TristanInput
        mark.TristanParam

* Methods to select particles and trace particle trajectories across multiple
  output files:

        mark.prtlindex
        mark.prtlindices
        mark.prtlselxyz
        mark.prtltraj
        mark.prtltraj_pts

* Methods to construct and manipulate tensors of shape (i, j; x, y, z).
  Convention: vector/tensor indices precede space (or spacetime) indices.

        tenpy.boost_em_flds
        tenpy.lorentz
        tenpy.rotmatrix

* Fourier transforms, spectrograms with a state-ful object-oriented interface

        tristanfldfft.TristanFldFFT
        tristanfldfft.TristanFldFFT.transform
        tristanfldfft.TristanFldFFT.transform
        tristanfldfft.one_sided_psd


Eponym
------

Tristan's uncle, Mark of Cornwall, who (usually?) kills Tristan in the legend.


See Also
--------

* [Iseult](https://github.com/pcrumley/Iseult)
* [tristanUtils](https://github.com/pcrumley/tristanUtils)
* IDL routines in `wid.pro` (ask Lorenzo Sironi)
